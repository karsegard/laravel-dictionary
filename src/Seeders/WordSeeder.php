<?php

namespace KDA\Dictionary\Seeders;

use Illuminate\Database\Seeder;

use KDA\Dictionary\Models\Word;

class WordSeeder extends \KDA\Backpack\Database\Seeders\CSVSeeder
{

    protected $has_headers=  false;
    protected $csv_separator= ";";
    public function beforeRun()
    {
    }
    public function getMap()
    {
        return ['word','lang'];
    }

    public function handleMap($line)
    {
        return $line;
    }

    public function getFile()
    {
        $file  = $this->getLastDumpFilename('french.csv');
        return $file;
    }


    public function afterRun()
    {
    }

    public function handleRow($row, $raw, $map)
    {
        $word = Word::create($row);
        

    }
}

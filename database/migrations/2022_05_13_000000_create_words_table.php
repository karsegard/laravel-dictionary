<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateWordsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::disableForeignKeyConstraints();

        Schema::create('words', function (Blueprint $table) {
            $table->id();
            $table->string('word');
            $table->string('lang');
            $table->string('accentless_hash')->nullable();
            $table->string('hash')->nullable();
            
            $table->timestamps();

        });

       

        Schema::enableForeignKeyConstraints();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::disableForeignKeyConstraints();
        Schema::dropIfExists('words');
        Schema::enableForeignKeyConstraints();
    }
}
